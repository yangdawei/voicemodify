% Copyright (c) 2014 Dawei Yang

% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

function [cep, y] = cepstrum(x, fs)
    N = length(x);
    x = x(:);

    window = hamming(N);
    x = x(:).*window(:);
    y = fft(x, N);

    cep = ifft(log(abs(y)));

    ms1 = fs / 1000;
    ms10 = fs / 100;

    q = (ms1:ms10) / fs;
    result = cep(ms1:ms10);
    plot(q, abs(result));
    legend('Cepstrum');

    xlabel('Quefrency (s) 1ms (1000Hz) to 10ms (100Hz)');
    ylabel('Amplitude');

    % f0 = pitch_cepstrum(cep, fs);
end