% Copyright (c) 2014 Dawei Yang

% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

function dist = pitch_distance(file1, file2)
    [y1, fs1] = audioread(file1);
    [y2, fs2] = audioread(file2);

    y1 = mean(y1, 2);
    y2 = mean(y2, 2);

    [a, b, c, d] = shrp(y1, fs1);
    b1 = b;
    [a, b, c, d] = shrp(y2, fs2);
    b2 = b;
    len = min(length(b1), length(b2));

    dist = sqrt(mean((b1(1:len) - b2(1:len)).^2));
end

