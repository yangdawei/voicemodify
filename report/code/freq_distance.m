% Copyright (c) 2014 Dawei Yang

% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

function dist = freq_distance(file1, file2)
    [y1, fs1] = audioread(file1);
    [y2, fs2] = audioread(file2);
    len = min(size(y1, 1), size(y2, 1));
    y1 = y1(1:len, :);
    y2 = y2(1:len, :);
    % [handle, s1] = myspectrogram(y1, fs1, [50, 1], @hamming, 1024);
    % [handle, s2] = myspectrogram(y2, fs2, [50, 1], @hamming, 1024);
    [f11, f21, f31] = formants(y1, fs1);
    [f12, f22, f32] = formants(y2, fs2);

    dist = (f11 - f12) .^2 + (f21 - f22) .^2 + (f31 - 32).^2;
    dist  = mean(dist) / 3;
    dist = sqrt(dist);

    % [handle, s1] = myspectrogram(y1, fs1);
    % [handle, s2] = myspectrogram(y2, fs2);
    % len = min(size(s1, 2), size(s2, 2));
    % s1 = s1(:, 1:len);
    % s2 = s2(:, 1:len);

    % dist = mean(mean((s1 - s2).^2));

    % mat = ones(1, len);

    % function dd = hi_dist(vec1, vec2)
    %     vec1 = vec1 / sum(vec1);
    %     vec2 = vec2 / sum(vec2);
    %     dd = 1 - sum(min(vec1, vec2)) / length(vec1);
    % end

    % for i = 1:len
    %     mat(i) = hi_dist(s1(:, i), s2(:, i));
    % end
    % dist = mean(mat);
end
