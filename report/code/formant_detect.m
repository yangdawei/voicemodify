% Copyright (c) 2014 Dawei Yang

% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:

% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.

% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

function [f1, f2, f3] = formant_detect(y, fs)

    % The formants are computed by solving for the roots of the LPC polynomial 
    %

    global f1p f2p f3p

    const=fs/(2*pi);
    x = lpc(y, 32);
    rts=roots(x);
    k=1;

    for i=1:length(x)-1
        re=real(rts(i)); im=imag(rts(i));
        formn=const*atan2(im,re);      %--formant frequencies
        bw=-0.5*const*log(abs(rts(i)));%--formant bandwidth
        if formn>90 & bw <500 & formn<4000
            save(k)=formn;
            bandw(k)=bw;
            k=k+1;
        end
            
    end

    [y, ind]=sort(save);



    %----Impose some formant continuity constraints -------------------

    leny=length(y);
    if leny==2, only2=1; else only2=0; end;

    in1=[]; in2=[];

    if ~isempty(f1p)
         in1=find(abs(f1p-y)<150);
         in2=find(abs(f2p-y)<150);
         if length(in1)>1, i1=in1(2); else, i1=in1; end;
         if length(in2)>1, i2=in2(2); else, i2=in2; end;

     
    if ~isempty(i1) & ~isempty(i2) 
        if i1==i2
            f1=y(1); f2=y(2); if only2==1, f3=f3p; else f3=y(3); end; 
        else
            f1=y(i1); f2=y(i2); 
            if i2+1 > leny, f3=f3p; else, f3=y(i2+1); end;

        end;
    elseif ~isempty(i1) & isempty(i2)
        f1=y(i1); f2=y(i1+1); 
        if i1+2 > leny, f3=f3p; else, f3=y(i1+2); end;
        
    else
       f1=y(1); f2=y(2); 
       if only2==1,    f3=f3p; 
       else        f3=y(3);
       end
      
    end


    else %++++++++++++++++ the first time ++++++++++++++++++
        if only2==1
            f1=300; f2=1200; f3=3000;
        elseif abs(y(1)-y(2))<80 
            f1=y(2); f2=y(3); if leny>3, f3=y(4); else, f3=3000; end;
        elseif leny==4
            f1=y(2); f2=y(3); f3=y(4); 
        else
            f1=y(1); f2=y(2); 
            if only2==1,    f3=3000; 
                else      f3=y(3);
            end
        end
    end

    % --last check .. --------
    if abs(f2-f3)<50
        f2=f2p; f3=f3p;
    end

    if abs(f2-f1)<50
        f2=f2p; f3=f3p;
    end
      

    %y(1:leny)
    %fprintf('%4.2f %4.2f %4.2f\n',f1,f2,f3);
    f1p=f1; f2p=f2; f3p=f3;
end