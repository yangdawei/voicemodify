names = {'sen6000', 'sen6015', 'sen6028', 'sen6044', 'sen6147'};
for i = 1:5
    before = ['../data/original/A/' names{i} '.wav'];
    after = ['../data/exp2_result/' names{i} '_syn.wav'];
    dest = ['../data/original/B/' names{i} '.wav'];
    disp(names{i});
    disp('pitch_distance:');
    pitch_distance(before, dest)
    pitch_distance(after, dest)
    disp('formant_distance:');
    freq_distance(before, dest)
    freq_distance(after, dest)
end
